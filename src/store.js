import Vue from 'vue';
import Vuex from 'vuex';
import createPersistedState from "vuex-persistedstate";

Vue.use(Vuex);

const state = {
  posts: null,
};

const mutations = {
  savePosts(_state, posts) {
    _state.posts = posts;
  }
};

const actions = {
  savePosts: ({ commit }) => commit('savePosts')
};

const getters = {
  getPosts: () => state.posts
};

export default new Vuex.Store({
  state,
  plugins: [createPersistedState()],
  getters,
  actions,
  mutations
});
