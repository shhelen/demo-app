import Vue from 'vue';
import App from './App.vue';
import VueRouter from 'vue-router';
import Home from './components/Home.vue';
import About from './components/About.vue';
import Post from './components/Post.vue';
import store from './store';

const routes = [
  { path: '/', redirect: { name: 'home' } },
  { path: '/home', name: 'home', component: Home },
  { path: '/about', name: 'about', component: About },
  { path: '/post/:id', name: 'post', component: Post },
  { path: '*', redirect: '/' }
];

const router = new VueRouter({ routes });

Vue.config.productionTip = false;

Vue.use(VueRouter);

new Vue({
  render: h => h(App),
  router,
  store,
  data: {
    device: 'desktop'
  },
  methods: {
    setDeviceType() {
      const windowWidth = window.innerWidth;
      let device = 'desktop';

      if (windowWidth >= 768 && windowWidth <= 992) {
        device = 'tablet';
      } else if (windowWidth <= 768) {
        device = 'mobile';
      }

      this.device = device;
      document.body.className += ` ${device}`;
    }
  },
  mounted() {
    this.setDeviceType();
  }
}).$mount('#app')
